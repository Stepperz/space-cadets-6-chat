package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by os4g14 on 11/11/2014.
 */
public class Connection {

    private PrintWriter out;
    private BufferedReader in;
    private Socket socket;

    private Queue<String> received;
    private String username = "Anonymous";
    private boolean isConnected = true;

    public Connection(Socket s){
        this.socket = s;
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        received = new LinkedList<>();

        listenForData();
    }

    private void listenForData(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String input;
                try {
                    while ((input = in.readLine()) != null) {
                        received.add(input);
                    }
                } catch (SocketException e) {
                    if(!socket.isClosed()){
                        received.add(username + " left the chat.");
                        isConnected = false;
                    }else {
                        e.printStackTrace();
                    }
                }catch (IOException e) {
                    if(!socket.isClosed()){
                        received.add(username + " left the chat.");
                        isConnected = false;
                    }else {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }

    public void sendString(String s){
        out.println(s);
    }

    public String getInputLine(){
        synchronized (received) {
            if (received.size() > 0) {
                return received.poll();
            }
            return null;
        }
    }

    public boolean isConnected(){
        return isConnected;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String name){
        username = name;
        received.add(username + " joined the chat.");
    }

}
