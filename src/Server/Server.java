package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by os4g14 on 11/11/2014.
 */
public class Server {

    //Hardcoded variables
    //TODO: remove these later
    private int hostPort = 8073;

    private ServerSocket socket;
    private ArrayList<Connection> clients = new ArrayList<>();

    private ArrayList<String> users = new ArrayList<>();

    public Server(){
        try {
            socket = new ServerSocket(hostPort);
        } catch (IOException e) {
            System.err.println("Error: Cannot open socket on port " + hostPort + ".");
            System.exit(-1);
        }

        AcceptConnections();

        StartChat();
    }

    private void AcceptConnections(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        final Socket c = socket.accept();
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Connection con = new Connection(c);
                                String name = "";
                                boolean invalid = true;
                                while(invalid) {
                                    invalid = false;
                                    while (true) {
                                        if ((name = con.getInputLine()) != null)
                                            break;
                                    }
                                    for (String s : users){
                                        if(s.equals(name)) {
                                            con.sendString("I");
                                            invalid = true;
                                            break;
                                        }
                                    }
                                }
                                users.add(name);
                                con.sendString("V");
                                con.setUsername(name);
                                synchronized (clients) {
                                    clients.add(con);
                                }
                            }
                        });
                        t.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }

    private void StartChat(){
        ArrayList<String> toSend = new ArrayList<>();
        String current;
        while(true){
            synchronized (clients) {
                for (int i = 0; i < clients.size(); i++) {
                    if(!clients.get(i).isConnected()) {
                        while ((current = clients.get(i).getInputLine()) != null) {
                            toSend.add(current);
                            System.out.println(current);
                        }
                        users.remove(clients.get(i).getUsername());
                        clients.remove(i);
                        i--;
                    }
                }

                for (Connection c : clients) {
                    while ((current = c.getInputLine()) != null) {
                        toSend.add(current);
                        System.out.println(current);
                    }
                }

                for(String s : toSend){
                    for (Connection c : clients){
                        c.sendString(s);
                    }
                }
                toSend.clear();
            }
        }
    }

    public static void main(String[] args){
        Server s = new Server();
    }

}
