package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by os4g14 on 11/11/2014.
 */
public class Client extends JFrame{

    //Hardcoded variables
    //TODO: remove these later
    private String hostAddress = "therealmgames.co.uk";
    private int hostPort = 8073;

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    private String username = "";

    private JSplitPane splitPane;
    private JScrollPane messageScrollPane;
    private JScrollPane inputScrollPane;
    private JTextArea messageArea;
    private JTextArea inputArea;

    private ActionMap actionMap;
    private InputMap inputMap;

    private String lastSent = "";

    public Client(){

        super();
        setPreferredSize(new Dimension(600, 450));
        setMinimumSize(new Dimension(600, 450));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        messageArea = new JTextArea();
        messageArea.setEditable(false);
        messageArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
        messageArea.setMargin(new Insets(5,5,5,5));
        messageArea.setLineWrap(true);

        messageScrollPane = new JScrollPane(messageArea);
        messageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        messageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageScrollPane.setMinimumSize(new Dimension(600, 300));

        inputArea = new JTextArea();
        inputArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
        inputArea.setEditable(true);
        inputArea.setMargin(new Insets(5,5,5,5));
        inputArea.setLineWrap(true);
        inputArea.setBorder(BorderFactory.createTitledBorder("Message"));

        inputMap = inputArea.getInputMap();
        inputMap.put(KeyStroke.getKeyStroke("ENTER"), "Send");
        actionMap = inputArea.getActionMap();
        actionMap.put("Send", Send);

        inputScrollPane = new JScrollPane(inputArea);
        inputScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        inputScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        inputScrollPane.setMinimumSize(new Dimension(600, 150));

        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, messageScrollPane, inputScrollPane);
        splitPane.setContinuousLayout(true);

        getContentPane().add(splitPane);
        setVisible(true);
        setTitle("Space Cadet Chat : Not Logged In");

        //Initialise the socket and the IO streams.
        try {
            socket = new Socket(hostAddress, hostPort);

            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }catch (UnknownHostException e) {
            writeToMessageArea("Error: Cannot connect to host '" + hostAddress + "'.");
            System.exit(-1);
        }catch (IOException e) {
            writeToMessageArea("Error: Cannot connect to host '" + hostAddress + "'.");
            System.exit(-1);
        }

        //Ask for a username
        try {
            while(true) {
                writeToMessageArea("Please enter a username:");
                while(true){
                    synchronized (lastSent) {
                        if (lastSent != "")
                            break;
                    }
                }

                String s = in.readLine();
                if (s.equals("I")) {
                    writeToMessageArea("That user is already connected.");
                    lastSent = "";
                    continue;
                }
                if (s.equals("V")) {
                    writeToMessageArea("Welcome " + lastSent);
                    username = lastSent;
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        setTitle("Space Cadet Chat : " + username);
        username += ":";

        //While loop to display any incoming messages
        String input;
        String message = "";
        try {
            while((input = in.readLine()) != null){
                String[] parts = input.split(":");
                if(parts.length > 2) {
                    for (int i = 2; i < parts.length; i++) {
                        message += parts[i];
                    }
                    writeToMessageArea("[" + parts[1] + "] > " + message);
                    message = "";
                }else{
                    writeToMessageArea("[ADMIN] > " + input);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToMessageArea(String s){
        messageArea.append(s + "\n");
    }

    Action Send = new AbstractAction("Send") {
        public void actionPerformed(ActionEvent e) {
            String input =inputArea.getText();
            if(username != "") {
                if(input.length() != 0)
                    send("ALL:" + username + input);
            }else
                send(input);
            writeToMessageArea(inputArea.getText());
            synchronized (lastSent) {
                lastSent = inputArea.getText();
            }
            inputArea.setText("");
            inputArea.setCaretPosition(0);
        }
    };

    private void send(String s){
        out.println(s);
    }

    public static void main(String[] args){
        Client c = new Client();
    }

}
